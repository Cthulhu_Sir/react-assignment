import React from 'react';
import { Component } from 'react';
import './App.css';
import 'froala-editor/css/froala_editor.pkgd.min.css';
import * as FroalaEditor from 'froala-editor/js/froala_editor.pkgd.min.js'
import 'froala-editor/js/plugins/image.min.js'

class Editor extends Component {

  URL = 'http://localhost:8080/image';

  render() {
    return (
      <div id="editor"></div>
    );
  }
  componentDidMount() {
    new FroalaEditor('#editor', {
      imageUploadMethod: 'POST',
      imageUploadURL: URL,
      imageEditButtons: [
        'imageReplace', 
        'imageAlign', 
        'imageRemove', 
        '|', 
        'imageLink', 
        'linkOpen', 
        'linkEdit', 
        'linkRemove', 
        '-', 
        'imageDisplay', 
        'imageAlt', 
        'imageSize',
        'imageCaption'
      ]
    });
  }
}

export default Editor;
