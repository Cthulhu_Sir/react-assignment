package com.svgubin.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.froala.editor.Image;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@WebServlet("/image")
@MultipartConfig
public class ImageUpload extends HttpServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String fileRoute = "/frontend/public/img/";
        Map<Object, Object> responseData;
        try {
            responseData = Image.upload(req, fileRoute);
        } catch (Exception e) {
            e.printStackTrace();
            responseData = new HashMap<Object, Object>();
            responseData.put("error", e.toString());
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ObjectMapper mapper = new ObjectMapper();

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        responseData.replace("link", "http://" + req.getHeader("host") + responseData.get("link"));
//        responseData.replace("link", "http://" + "10.200.126.146:8080" + responseData.get("link"));
        resp.getWriter().write(mapper.writeValueAsString(responseData));
    }
}
