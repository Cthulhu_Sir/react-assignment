package com.svgubin.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.svgubin.entity.Article;
import com.svgubin.service.ArticleService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet("/articles")
public class ArticlesListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doGet(req, resp);
        int limit = Integer.parseInt(req.getParameter("limit"));
        int offset = Integer.parseInt(req.getParameter("offset"));

//        ObjectMapper mapper = new ObjectMapper();

        ArticleService articleService = new ArticleService();
        List<Article> articles = articleService.getLatestArticleList(limit, offset);

        resp.setContentType("application/json");
        resp.setCharacterEncoding("UTF-8");
        resp.getWriter().write(articles.toString());

    }
}
