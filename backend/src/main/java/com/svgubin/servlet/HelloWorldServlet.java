package com.svgubin.servlet;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

@WebServlet("/TEST")
public class HelloWorldServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println(req.getReader().readLine());
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        super.doPost(req, resp);

        //Создаём маппер из Jackson'а
        ObjectMapper mapper = new ObjectMapper();

        //забираем ридер из реквеста, чтобы "Читать" тело запроса
        //Если у нас x-www-form-urlencoded, то можем просто вытаскивать параметры
        BufferedReader reader = req.getReader();

        //Строка с итоговым json'ом и строка в которую будем "читать" тело запроса
        String json = new String();
        String line;

        //Читаем тело запроса
        while ((line = reader.readLine()) != null){
            json += line;
        }

        System.out.println(json);

        //Создаём новую мапу в которую парсим json
        Map<String, String> map = mapper.readValue(json, Map.class);

        System.out.println(map);

        map.put("Key", "Value");

        //Получаем Writer чтобы записать тело ответа
        PrintWriter out = resp.getWriter();

        //Устанавливаем "тип" ответа
        resp.setContentType("application/json");

        //И его кодировку
        resp.setCharacterEncoding("UTF-8");

        //Делаем принт мапы прогнанной через стрингифаер
        out.print(mapper.writeValueAsString(map));

        //Очищаем райтер
        out.flush();
    }
}
