package com.svgubin.model;

import com.fasterxml.jackson.databind.ObjectMapper;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class HttpRequest {

    private HttpServletRequest req;

    private String paramsStringified;
    private Map parameters;//////////////То же самое замечание

    private Map<String, String> headers;
    private String method;

    private Map<String, String> cookiesMap;/////????????????????
    private List<Cookie> cookies;

    private class ContentType {
        public static final String JSON = "application/json";
        public static final String URLENCODED = "application/x-www-form-urlencoded";
        public static final String XML = "application/xml";////////???????????????
    }

    private class Method {
        public static final String GET = "GET";
        public static final String POST = "POST";
        public static final String PUT = "PUT";
        public static final String DELETE = "DELETE";
    }


    /**
     *
     */
    private void parseParams() {
        this.parameters = this.req.getParameterMap();
        //STRINGIFY
    }


    /**
     *
     * @throws IOException
     */
    private void processPost() throws IOException {
        if (headers.get("content-type") == ContentType.URLENCODED) {

            this.parseParams();

        } else if (headers.get("content-type") == ContentType.JSON) {

            this.parseJsonBody();

        } else {

            this.parameters = new HashMap<String, String>();

        }
    }


    /**
     * This method
     *
     * @throws IOException
     */
    private void parseJsonBody() throws IOException {
        BufferedReader reader = req.getReader();
        String line;
        paramsStringified = "";

        while ((line = reader.readLine()) != null) {
            this.paramsStringified += line;
        }

        ObjectMapper mapper = new ObjectMapper();

        this.parameters = mapper.readValue(this.paramsStringified, Map.class);
    }


    /**
     *
     * @param req
     * 
     * @throws IOException
     */
    public HttpRequest(HttpServletRequest req) throws IOException {
        this.req = req;

        Enumeration<String> headersNames = this.req.getHeaderNames();

        while (headersNames.hasMoreElements()) {
            String headerName = headersNames.nextElement();
            String headerContent = this.req.getHeader(headerName);
            this.headers.put(headerName, headerContent);
        }


        this.method = req.getMethod();

        if (this.method == Method.GET) {

            this.parseParams();

        } else if (this.method == Method.POST) {

            this.processPost();

        } else {
            throw new NotImplementedException();
        }

    }

}
